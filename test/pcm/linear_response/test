#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

f_hf = Filter()
f_hf.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_hf.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_hf.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_hf.add(string = 'Final HF energy:',
      rel_tolerance = 1.0e-9)
f_hf.add(from_string = 'POLARIZABILITY',
      to_string   = 'End of polarizability calculation',
      ignore_below = 1.0e-7,
      rel_tolerance = 1.0e-7)

# Equilibrium PCM formulation
test.run(['static_alpha_hf.dal', 'dynamic_alpha_hf.dal'], ['H2O.mol'], {'out': f_hf}, ['eq_pcmsolver.pcm'])
# Nonequilibrium PCM formulation
test.run(['dynamic_alpha_hf.dal'], ['H2O.mol'], {'out': f_hf}, ['noneq_pcmsolver.pcm'])

f_hfOPA = Filter()
f_hfOPA.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_hfOPA.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_hfOPA.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_hfOPA.add(string = 'Final HF energy:',
      rel_tolerance = 1.0e-9)
f_hfOPA.add(from_string = 'ONE-PHOTON ABSORPTION',
      to_string   = 'End of excitation energy calculation',
      ignore_sign = True,
      abs_tolerance = 1.0e-4)
# Nonequilibrium PCM formulation
test.run(['opa_hf.dal'], ['H2O.mol'], {'out': f_hfOPA}, ['noneq_pcmsolver.pcm'])

f_lda = Filter()
f_lda.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_lda.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_lda.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_lda.add(string = 'Final DFT energy:',
      rel_tolerance = 1.0e-9)
f_lda.add(from_string = 'POLARIZABILITY',
      to_string   = 'End of polarizability calculation',
      ignore_below = 1.0e-7,
      rel_tolerance = 1.0e-7)
# Equilibrium PCM formulation
test.run(['static_alpha_lda.dal', 'dynamic_alpha_lda.dal'], ['H2O.mol'], {'out': f_lda}, ['eq_pcmsolver.pcm'])
# Nonequilibrium PCM formulation
test.run(['dynamic_alpha_lda.dal'], ['H2O.mol'], {'out': f_lda}, ['noneq_pcmsolver.pcm'])

f_ldaOPA = Filter()
f_ldaOPA.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_ldaOPA.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_ldaOPA.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_ldaOPA.add(string = 'Final DFT energy:',
      rel_tolerance = 1.0e-9)
f_ldaOPA.add(from_string = 'ONE-PHOTON ABSORPTION',
      to_string   = 'End of excitation energy calculation',
      ignore_sign = True,
      abs_tolerance = 1.0e-4)
# Nonequilibrium PCM formulation
test.run(['opa_lda.dal'], ['H2O.mol'], {'out': f_ldaOPA}, ['noneq_pcmsolver.pcm'])

sys.exit(test.return_code)
