#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_UHF_hessian.info <<'%EOF%'
   LSDALTON_UHF_hessian
   -------------
   Molecule:         CH3BF
   Wave Function:    HF/3-21G
   Test Purpose:     Check UHF hessian eigenvalue
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_UHF_hessian.mol <<'%EOF%'
BASIS
3-21G
-----------
-------------
Atomtypes=4 Nosymmetry Angstrom
Charge=6. Atoms=1      
C   -0.087698    0.588622    -0.070178
Charge=1. Atoms=3      
H   -0.177151    1.246255     0.769118
H    2.312537    0.265042    -0.512972
H    1.439897   -1.233513     0.568708
Charge=5. Atoms=1      
B    1.358936   -0.201653     0.002044
Charge=9. Atoms=1      
F   -1.093753   -0.311250    -0.046001
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_UHF_hessian.dal <<'%EOF%'
**INTEGRALS
.THRESH
1.D-10
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.STABILITY
.UNREST
.CONVDYN
TIGHT
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_UHF_hessian.check
cat >> LSDALTON_UHF_hessian.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-162\.747087" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT"

# Hessian eigenvalue
CRIT1=`$GREP "Hessian eigenvalue no\. * 1\: * 0?\.16977" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="HF HESSIAN EIGENVALUE NOT CORRECT"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
#  THIS TEST CASE WAS COMPARED TO GAUSSIAN USING
#
#
#%chk=weird.chk
#%mem=6MW
#%nproc=1
## td=(root=5) hf/3-21g geom=connectivity scf=conver=10
#
#Title Card Required
#
#0 2
# C
# H                  1              B1
# B                  1              B2    2              A1
# H                  3              B3    1              A2    2              D1
# H                  3              B4    1              A3    2              D2
# F                  1              B5    3              A4    4              D3
#
#   B1             1.07000000
#   B2             1.65000000
#   B3             1.18000000
#   B4             1.18000000
#   B5             1.35000000
#   A1           109.47120255
#   A2           120.00000011
#   A3           120.00000011
#   A4           109.47120255
#   D1           -90.01139260
#   D2            89.98860740
#   D3           149.98859260
#
# 1 2 1.0 3 1.0 6 1.0
# 2
# 3 4 1.0 5 1.0
# 4
# 5
# 6
#
# This gaussian run gives 
#
#  E(UHF) = -162.747087257
#
#  Excitation energies
#    4.8305 eV
#    5.4386 eV
#    6.0000 eV
#    6.5882 eV
#    6.9773 eV
# 
