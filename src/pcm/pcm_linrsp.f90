!> @file
!> LSDALTON-side interface routines for the Polarizable Continuum Model
module pcm_linrsp

use iso_c_binding
use typedeftype, only: lssetting
use pcm_config, only: pcmtype, PCM, pcm_cfg, pcm_global, context_
use pcm_precision
use pcmsolver
use pcm_integrals, only: get_electronic_mep
use integralinterfacemod, only: II_get_ep_ab
use matrix_module
use lstiming, only: lstimer

implicit none

public pcm_OITasc_mep

private

character(1)  :: mep_oit(7) = (/'O','I','T','M','E','P',char(0)/)
character(1)  :: asc_oit(7) = (/'O','I','T','A','S','C',char(0)/)

contains

!> \brief calculates PCM explicit contribution to sigma vector
!> \author R. Di Remigio
!> \date 2015
!> \param Gpcm the PCM contribution
!> \param Bmat the one-index transformed density matrix
!>
!> We have an implicit and an explicit PCM contribution
!> to the sigma vector. The implicit contribution comes
!> from the contraction of the unperturbed apparent surface charge
!> (ASC) with the one-index transformed (OIT) molecular
!> electrostic potential (MEP). This is implicit as it amounts
!> to one-index transform the usual Fock matrix contribution.
!> Notice that this is done automatically by LSDALTON.
!> The explicit contribution comes from the contraction of
!> the OIT-ASC with the unperturbed MEP.
!> For the explicit contribution, we might want to use
!> a nonequilibrium formulation.
subroutine pcm_OITasc_mep(Gpcm, Bmat)

   type(matrix), intent(inout) :: Gpcm
   type(matrix), intent(in)    :: Bmat

   integer(4)   :: irrep
   real(c_double), allocatable :: mep(:), asc(:)
   real(kind=dp) :: ts_mep, te_mep, ts_oper, te_oper
   real(kind=dp) :: ts_asc, te_asc

   ! The totally symmetric irrep
   irrep = 0
   call lstimer('START ', ts_mep, te_mep, pcm_global%global_print_unit)
   ! We first get the one-index transformed MEP
   allocate(mep(pcm_global%nr_points))
   mep = 0e0_dp
   call get_electronic_mep(int(pcm_global%nr_points),    &
                           pcm_global%tess_cent,         &
                           mep,                          &
                           Bmat,                         &
                           pcm_global%integral_settings, &
                           pcm_global%global_print_unit, &
                           pcm_global%global_error_unit)
   call pcmsolver_set_surface_function(context_, pcm_global%nr_points, mep, mep_oit)
   call lstimer('OITMEP', ts_mep, te_mep, pcm_global%global_print_unit)

   ! And now PCMSolver calculates the ASC
   call lstimer('START ', ts_asc, te_asc, pcm_global%global_print_unit)
   allocate(asc(pcm_global%nr_points))
   asc = 0e0_dp
   call pcmsolver_compute_response_asc(context_, mep_oit, asc_oit, irrep)
   call pcmsolver_get_surface_function(context_, pcm_global%nr_points, asc, asc_oit)
   call lstimer('OITASC', ts_asc, te_asc, pcm_global%global_print_unit)

   call lstimer('START ', ts_oper, te_oper, pcm_global%global_print_unit)
   call II_get_ep_ab(pcm_global%global_print_unit, &
                     pcm_global%global_error_unit, &
                     pcm_global%integral_settings, &
                     Gpcm,                         &
                     int(pcm_global%nr_points),    &
                     pcm_global%tess_cent,         &
                     asc)
   call lstimer('OITPCM', ts_oper, te_oper, pcm_global%global_print_unit)

   deallocate(mep)
   deallocate(asc)

end subroutine pcm_OITasc_mep

end module
